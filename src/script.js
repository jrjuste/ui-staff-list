//custom element class
(function () {
    //specify tag and classname
    class StaffList extends HTMLElement {
        constructor() {

            super();

            //attach shadow tree
            const shadow = this.attachShadow({ mode: 'open' });

            //creating a container for the list component
            const staffListContainer = document.createElement('div');

            //this variable will hold new members
            const addNewMember = this.addNewMember;

            //this variable will display current members
            const memberList = this.members;

            staffListContainer.classList.add('staff-list');
            
            //creating the innerHTML and simple styling for the list
            staffListContainer.innerHTML = `
            <style>
            h1 {
                font-size: 50px;
                color:green;
            }
            ul {
                list-style-type: circle;
            }
            </style>
            <h1>UX Team</h1>
            <ul class="member-list">
            ${memberList.map(member => `
            <li>${member}</li>`)}
            </ul>
            <div>
            <label>${addNewMember}</label>
            <input class="add-new-member-input" type="text"></input>
            <button class="staff-list-add-member">Add Team Member</button>
            </div>
            `;

            //binding method
            this.addTeamMember = this.addTeamMember.bind(this);

            //appending the container to the shadow DOM
            shadow.appendChild(staffListContainer);
        }
        //add members to list
        addTeamMember(e) {
            //variable for new team member
            const textInput = this.shadowRoot.querySelector('.add-new-member-input');

            //if a value exists for a team member
            if (textInput.value) {
                //create list item for new member
                const li = document.createElement('li');
                const childrenLength = this.teamList.children.length;

                //the name variable
                li.textContent = textInput.value;

                //append it to the list
                this.teamList.appendChild(li);
                //clear the field
                textInput.value = '';
            }
        }

        //fires after the element has been attached to the DOM
        connectedCallback() {
            //button variable
            const addElementButton = this.shadowRoot.querySelector('.staff-list-add-member');
            //ul variable
            this.teamList = this.shadowRoot.querySelector('.member-list');
            //listen for clicks, this has been binded to the page
            addElementButton.addEventListener('click', this.addTeamMember, false);
        }

        // gathering data from element attributes
        get members() {

            //hold my team members
            const members = [];

            //push team members into the array
            [...this.attributes].forEach(attr => {
                if (attr.name.includes('list-name')) {
                    members.push(attr.value);
                }
            });
            //return all members
            return members;
        }

        //this holds my label in getMembers function
        get addNewMember() {
            return this.getAttribute('add-new-member') || '';
        }
    }

    // register my custom element
    customElements.define('staff-list', StaffList);
})();