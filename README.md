# UI-staff-list
A simple list updated with a button in a web component

# sources
https://github.com/mdn/web-components-examples  
https://www.youtube.com/watch?v=PFpUCnyztJk
https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_templates_and_slots
https://opensource.com/article/21/7/web-components
